#!/usr/bin/python3

import json

from pathlib import Path

import re
import selinux
import sepolicy
import sys

def find_services(path='selinux-policy'):

    for file_fc in Path(path).glob('**/*.fc'):

        for line in open(file_fc, "r"):
            s = re.search(
                r'(/usr/lib/systemd/system/([a-zA-Z0-9-_]+)\\?\.\S+)\s+\S+\s+gen_context\(([^,]+),.*\)',
                line
            )
            if s:
                # (service, regexp, selinux_context)
                yield (s.group(2), s.group(1), s.group(3).split(':'))

try:
    policy_sources = sys.argv[1]
except:
    policy_sources = 'selinux-policy'

try:
    policy_xml = sys.argv[2]
except:
    policy_xml = 'policy.xml'

services = {}
bool_dict = sepolicy.gen_bool_dict(policy_xml)

for service in find_services(policy_sources):
    service_name = service[0]
    service_regexp = service[1]
    service_unit_label = service[2]
    service_unit_type = service_unit_label[2]
    macro_unit_file = "systemd_unit_file(" + service_unit_type + ")"

    services[service_name] = { 'regexp' : service_regexp, 'booleans' : {} }

    for te_file in Path(policy_sources).glob('**/*.te'):
        found = False
        for line in filter(lambda x: macro_unit_file in x, open(te_file, "r")):
            found = True
            break

        if not found:
            continue

        for line in filter(lambda x: "gen_tunable" in x, open(te_file, "r")):
            boolean = line[line.find('(') + 1: line.find(',')]
            services[service_name]['booleans'][boolean] = bool_dict[boolean][2]

    if len(services[service_name]['booleans']) == 0:
        del services[service_name]

print(json.dumps(services, sort_keys=True, indent=2))
